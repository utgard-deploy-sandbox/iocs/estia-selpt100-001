#!/usr/bin/env iocsh.bash
# Simple IOC to test the Modbus TCP/IP conenction with Beckhoff EK9000     #
#                                                                          #
# created by: Tamas Kerenyi WP12                                           #
# tamas.kerenyi@esss.se                                                    #
#                                                                          #
# **************************************************************************

require(modbusterminal)

#
#
# -----------------------------------------------------------------------------
# Configure ENVIRONMENT
# -----------------------------------------------------------------------------
# Default paths to locate database and protocol
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(beckhoff_mod_DIR)db/")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(beckhoff_mod_DIR)db/")


# -----------------------------------------------------------------------------
# Asyn IP configuration
# -----------------------------------------------------------------------------
drvAsynIPPortConfigure("Bck","192.168.1.129:502",0,0,1)
# -----------------------------------------------------------------------------
# Modbus configuration
modbusInterposeConfig("Bck",0,1000,0)
# -----------------------------------------------------------------------------
#drvModbusAsynConfigure(portName, 
#                       tcpPortName,
#                       slaveAddress, 
#                       modbusFunction, 
#                       modbusStartAddress, 
#                       modbusLength,
#                       dataType,
#                       pollMsec, 
#                       plcType);

# -----------------------------------------------------------------------------
# HoldingRegisterPort
# -----------------------------------------------------------------------------
drvModbusAsynConfigure(HoldingRegisterPort,Bck,0,4,0,28,0,1000,"Beckhoff");


# -----------------------------------------------------------------------------
# WriteMultipleRegisterPort
# -----------------------------------------------------------------------------
drvModbusAsynConfigure(WriteMultipleRegisterPort,Bck,0,16,5120,7,0,1000,"Beckhoff");

# -----------------------------------------------------------------------------
# ReadMultipleRegisterPort
# -----------------------------------------------------------------------------
drvModbusAsynConfigure(ReadMultipleRegisterPort,Bck,0,3,5120,7,0,1000,"Beckhoff");

# -----------------------------------------------------------------------------
# Load record instances


dbLoadRecords("beckhoff_mod.db","P=estia, PORT=modbus-stream, R=-selpt100-001:, A=-1")

iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

iocInit()

dbpf estia-selpt100-001:ReadMultipleRegisters_00 0
dbpf estia-selpt100-001:ReadMultipleRegisters_01 1


dbpf estia-selpt100-001:Value_1 26
dbpf estia-selpt100-001:Value_2 25
dbpf estia-selpt100-001:Value_3 2
dbpf estia-selpt100-001:Value_4 21
